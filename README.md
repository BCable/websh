websh v0.1a Documentation
=========================


Table of Contents
-----------------

    Introduction

    I: Usage


Introduction
------------

WebSH is a script that executes commands on a remote machine without opening a
port, but by uploading a PHP file. This script currently only works for Unix
based servers and Unix based clients.

This script emphasizes the fact that a vulnerability that allows a file to be
edited or uploaded is worse than it may seem.


I. Usage
--------

    websh <url>

The url is the location of the script.  "http://" at the beginning is optional.

Example execution:
    websh example.com/websh.php
    websh http://example.com/websh.php
